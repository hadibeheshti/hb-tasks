using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ScoreManager : MonoBehaviour
{
    [SerializeField] GameObject scoreGO;
    [SerializeField] Text scoreText;
    [SerializeField] int score;

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        scoreGO = GameObject.FindGameObjectWithTag("Score");
        scoreText = scoreGO.GetComponent<Text>();
        scoreText.text = score.ToString();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            score++;
            scoreText.text = score.ToString();
        }
    }
}
