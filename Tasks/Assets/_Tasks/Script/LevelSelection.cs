using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class GameMode
{
    public string gameName;
    public string description;
    public string msg;
    public Sprite previewImage;
    public bool isUnlocked;
    public int score;

}

public class LevelSelection : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI gameNameText;
    [SerializeField] TextMeshProUGUI descriptionText;
    [SerializeField] GameObject msgHolder;
    [SerializeField] TextMeshProUGUI msgText;
    [SerializeField] Image previewImage;
    [SerializeField] Button playButton;
    [SerializeField] GameMode[] gameModes;
    [SerializeField] int highScore;

    private int currentGameIndex = 0;


    private void Start()
    {
        highScore = 0;
        UpdateUI();

    }

    public void LoadLevel()
    {
        if (gameModes[currentGameIndex].isUnlocked)
        {
            // Load the selected game mode scene
            string sceneName = "GameMode" + currentGameIndex; 
            SceneManager.LoadScene(sceneName);
        }

    }

    public void NextGame()
    {
        currentGameIndex = (currentGameIndex + 1) % gameModes.Length;
        UpdateUI();
    }

    public void PreviousGame()
    {
        currentGameIndex = (currentGameIndex - 1 + gameModes.Length) % gameModes.Length;
        UpdateUI();

    }

    private void UpdateUI()
    {
        if (highScore >= gameModes[currentGameIndex].score)
        {
            msgHolder.SetActive(false);
            msgText.text = "";
            gameModes[currentGameIndex].isUnlocked = true;
        }
        else
        {
            msgHolder.SetActive(true);
            msgText.text = gameModes[currentGameIndex].msg + gameModes[currentGameIndex].score;
            gameModes[currentGameIndex].isUnlocked = false;
        }

        gameNameText.text = gameModes[currentGameIndex].gameName;
        descriptionText.text = gameModes[currentGameIndex].description;
        previewImage.sprite = gameModes[currentGameIndex].previewImage;
        playButton.interactable = gameModes[currentGameIndex].isUnlocked;
    }
}
